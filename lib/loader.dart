import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

Future<List<Recipe>> fetchRecipes() async {
  final response = await Dio().get('https://coffeebrewapi.alphawave.ru');

  return compute(parseRecipes, response.toString());
}

List<Recipe> parseRecipes(String responseBody) {
  final parsed = jsonDecode(responseBody)['data'];

  return parsed.map<Recipe>((json) => Recipe.fromJson(json)).toList();
}

class Recipe {
  final String name;

  Recipe({required this.name});

  factory Recipe.fromJson(Map<String, dynamic> json) =>
      Recipe(name: json['name'].toString());
}
