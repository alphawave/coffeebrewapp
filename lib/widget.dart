import 'package:coffeebrewapp/RecipeDetails/widget.dart';
import 'package:coffeebrewapp/loader.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: FutureBuilder<List<Recipe>>(
          future: fetchRecipes(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              print(snapshot.error);
            }

            return snapshot.hasData
                ? RecipesList(recipes: snapshot.data!)
                : Center(child: CircularProgressIndicator());
          },
        ),
      );
}

class RecipesList extends StatelessWidget {
  final List<Recipe> recipes;

  RecipesList({Key? key, required this.recipes}) : super(key: key);

  @override
  Widget build(BuildContext context) => ListView.builder(
      itemCount: recipes.length,
      itemBuilder: (context, index) => showRecipe(context, recipes[index]));

  Widget showRecipe(BuildContext context, Recipe recipe) => Container(
        child: GestureDetector(
          onTap: () => {navigateToRecipeDetails(context, recipe.name)},
          child: Padding(
            padding: EdgeInsets.all(8),
            child: Text(
              recipe.name,
              softWrap: true,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: 40, color: Theme.of(context).primaryColor),
            ),
          ),
        ),
      );

  void navigateToRecipeDetails(BuildContext context, String name) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => RecipePage(name: name)));
  }
}
