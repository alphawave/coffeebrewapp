import 'package:coffeebrewapp/widget.dart';
import 'package:flutter/material.dart';

void main() => runApp(CBApp());

class CBApp extends StatefulWidget {
  @override
  _CBAppState createState() => _CBAppState();
}

class _CBAppState extends State<CBApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Coffee Brew App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Coffee Brew App'),
    );
  }
}
