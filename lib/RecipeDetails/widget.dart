import 'package:coffeebrewapp/RecipeDetails/loader.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RecipePage extends StatelessWidget {
  RecipePage({Key? key, required this.name}) : super(key: key);

  final String name;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(this.name),
        ),
        body: FutureBuilder<Recipe>(
          future: fetchRecipe(this.name),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);

            return snapshot.hasData
                ? RecipeDetails(recipe: snapshot.data!)
                : Center(child: CircularProgressIndicator());
          },
        ),
      );
}

class RecipeDetails extends StatelessWidget {
  final Recipe recipe;

  RecipeDetails({Key? key, required this.recipe}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        child: Padding(
            padding: EdgeInsets.all(8),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                recipe.name,
                softWrap: true,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 40, color: Theme.of(context).primaryColor),
              ),
              Text(recipe.description!, softWrap: true),
              Text(recipe.ingredients!, softWrap: true),
            ])),
      );
}
