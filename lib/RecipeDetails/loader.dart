import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

Future<Recipe> fetchRecipe(String name) async {
  final response =
      await Dio().get('https://coffeebrewapi.alphawave.ru/' + name);

  return compute(parseRecipe, response.toString());
}

Recipe parseRecipe(String responseBody) {
  final parsed = jsonDecode(responseBody)['data'];

  return Recipe.fromJson(parsed);
}

class Recipe {
  final String name;
  final String? description;
  final String? ingredients;

  Recipe(
      {required this.name,
      required this.description,
      required this.ingredients});

  factory Recipe.fromJson(Map<String, dynamic> json) => Recipe(
      name: json['name'].toString(),
      description:
          (json['description'] != null) ? json['description'].toString() : '',
      ingredients: '11');
}
